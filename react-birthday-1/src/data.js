/* eslint-disable import/no-anonymous-default-export */
export default [
    {
        id: 1,
        fullName: 'John Smith',
        age: 29,
        image: 'https://i.ibb.co/Lx5y9YZ/avatar-with-bro.jpg',
    },
    {
        id: 2,
        fullName: 'John Doe',
        age: 25,
        image: 'https://i.ibb.co/Lx5y9YZ/avatar-with-bro.jpg',
    }
]