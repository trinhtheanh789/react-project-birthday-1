import React from 'react';

const List = ({people }) => {
    return (
        <>
            {people.map((person)=>{
                const {id, fullName, age, image} =person;
                return (
                    <article key={id} className="person">
                        <img src={image} alt={fullName} className="person-img"/>
                        <div>
                            <h4>{fullName}</h4>
                            <p>{age} years old</p>
                        </div>
                    </article>
                );
            })}
        </>
    );
};

export default List;