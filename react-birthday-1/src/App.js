import React, { useState } from 'react';
import data from './data';
import List from './List';

function App() {
  const [people, setPeople] = useState(data);
  return (
    <section className="container">
      <h3>{people.length} birthdays today</h3>
      <hr className="hr-half"/>
        <List people={people}/>
        <button onClick={() => setPeople([])} className="btn-primary ctm-btn">Clear list</button>
    </section>
  );
}

export default App;
